# Robert's Conan Package Hacks

A collection of Conan recipes modified for whatever reason. Mainly to make cross building for Windows easier.

Recipes are taken from the [Conan Index](https://github.com/conan-io/conan-center-index/) unless otherwise stated.

## Exporting the libraries

Use conan create with the appropriate cross building profile as needed.

i.e.

```bash
cd boost
conan create . --version=1.81.0 --user=me --channel=hacks -pr:h=clang-crossbuild -pr:b=default --build=missing
```

## Boost

This hack changes the toolset detection to always use clang-win for MSVC. 

There's not really a good way to detect this idiomatically, as far as I can tell.

To compile boost, you will need to create a wrapper script around clang-cl to change the linker:

```bash
#!/bin/bash

clang-cl -fuse-ld=lld $@
```

Boost (or rather b2) doesn't expose an idiomatic way to do this.
